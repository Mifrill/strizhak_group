<?php
require 'build.php';

echo $twig->render('head.html', array(
  'title' => 'Strizhak Group',
  'link_get_started' => '#about',
  'text_get_started' => 'Get Strated',
  'link_Our_Servives' => '#services',
  'text_Our_Servives' => 'Our Services',
));

echo $twig->render('index.html');

echo $twig->render('footer.html');
