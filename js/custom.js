jQuery(document).ready(function( $ ) {
  $.getJSON('data.json', {_: new Date().getTime()}, function(dataJs) {
    // Preloader
    $(window).on('load', function() {
      $('#preloader').delay(100).fadeOut('slow',function(){$(this).remove();});
    });

    // Hero rotating texts
    $("#hero .rotating").Morphext({
      animation: "flipInX",
      separator: ",",
      speed: 3000
    });
    
    // Initiate the wowjs
    new WOW().init();
    
    // Initiate superfish on nav menu
    $('.nav-menu').superfish({
      animation: {opacity:'show'},
      speed: 400
    });
    
    // Mobile Navigation
    if( $('#nav-menu-container').length ) {
        var $mobile_nav = $('#nav-menu-container').clone().prop({ id: 'mobile-nav'});
        $mobile_nav.find('> ul').attr({ 'class' : '', 'id' : '' });
        $('body').append( $mobile_nav );
        $('body').prepend( '<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>' );
        $('body').append( '<div id="mobile-body-overly"></div>' );
        $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');
        
        $(document).on('click', '.menu-has-children i', function(e){
            $(this).next().toggleClass('menu-item-active');
            $(this).nextAll('ul').eq(0).slideToggle();
            $(this).toggleClass("fa-chevron-up fa-chevron-down");
        });
        
        $(document).on('click', '#mobile-nav-toggle', function(e){
            $('body').toggleClass('mobile-nav-active');
            $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
            $('#mobile-body-overly').toggle();
        });
        
        $(document).click(function (e) {
            var container = $("#mobile-nav, #mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
               if ( $('body').hasClass('mobile-nav-active') ) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
                    $('#mobile-body-overly').fadeOut();
                }
            }
        });
    } else if ( $("#mobile-nav, #mobile-nav-toggle").length ) {
        $("#mobile-nav, #mobile-nav-toggle").hide();
    }
    
    // Stick the header at top on scroll
    $("#header").sticky({topSpacing:0, zIndex: '50'});

    // Smoth scroll on page hash links
    $('a[href*="#"]:not([href="#"])').on('click', function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            if (target.length) {
                
                var top_space = 0;
                
                if( $('#header').length ) {
                  top_space = $('#header').outerHeight();
                }
                
                $('html, body').animate({
                    scrollTop: target.offset().top - top_space
                }, 1500, 'easeInOutExpo');

                if ( $(this).parents('.nav-menu').length ) {
                  $('.nav-menu .menu-active').removeClass('menu-active');
                  $(this).closest('li').addClass('menu-active');
                }

                if ( $('body').hasClass('mobile-nav-active') ) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
                    $('#mobile-body-overly').fadeOut();
                }
                
                return false;
            }
        }
    });
    
    // Back to top button
    $(window).scroll(function() {

        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
        
    });
    
    $('.back-to-top').click(function(){
        $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
        return false;
    });
    
  /* Ajax Form for consultations */
    $("#form-proposal").submit(function() {
      var form = $(this);
      var error = false;
  /* validation on client side */
      form.find('input').each( function(){ // прoбeжим пo кaждoму пoлю в фoрмe
        if ($(this).val() == '') { // eсли нaхoдим пустoe
          $('.alert').remove();
          $('.form').prepend("<div class='alert alert-warning'></div>");
          $('.alert-warning').prepend('"'+$(this).attr('placeholder')+'" !');
          error = true; // oшибкa
        }
      });
  /* ajax if No error */
      if (!error) {
        $.ajax({
          type: "POST",
          url: "./mail.php",
          dataType: 'json',
          data: $(this).serialize(),
        }).done(function(data) {
          if (data['error']) { // if php script get error on server side
            $('.alert').remove();
            $('.form').prepend("<div class='alert alert-danger'></div>");
            $('.alert-danger').prepend('"'+data['error']+'" !'); // show error from placeholder
          } else { // if no error from server side
            //$(this).find("input").val("");
            $('.alert').remove();
            $('.form').prepend("<div class='alert alert-success'></div>");
            $('.alert-success').prepend('Thank You. We will call you soon');
            $('button[type="submit"]').empty();
            $('button[type="submit"]').append('Application submitted');
            $('button[type="submit"]').attr('disabled','disabled');
          }
  /* if spicify error from server */
        }).fail(function(xhr, thrownError) {
          $('.alert').remove();
          $('.form').prepend("<div class='alert alert-danger'></div>");
          $('.alert-danger').prepend('SERVER ERROR: '+xhr.status +'  '+ thrownError);
        });
      }
      return false;
    });
  }); 
});
